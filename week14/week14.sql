#initial time:0.359 sec
#after index:0.203 sec
#after PK:0.157 sec
use uniprot_example;

show variables like "%batch%";
use uniprot_example;

select * from proteins;
dd a comment to this line

load data local infile 'C:\\Users\\Cagatay\\Desktop\\insert.txt' into table proteins fields terminated by '|';proteins`PRIMARY`

delete from proteins;

select *
from proteins
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

drop index uniprot_index on proteins;
create index uniprot_index on proteins (uniprot_id);

alter table proteins add constraint pk_proteins primary key (uniprot_id);
alter table proteins drop primary key;